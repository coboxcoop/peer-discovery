
May, 2021

Published by Magma Collective
info@magmacollective.org


# Summary

This report collates the results of a second round of needfinding workshops, with a total of 9 people from 7 different organisations. The main aim of the workshops was to investigate the needs and requirements for a ‘peer discovery’ mechanism in CoBox. This required gaining a deeper understanding of how organisations tend to find one another, what builds trust between them, and what kind of information or support is desirable in order for organisations to be able and willing to backup one another's data. In conclusion, we found that the type of organisations who represent early users of CoBox tend to be highly networked and to build trust outside of technical networks. In order for this 'human trust' to translate into 'technical trust', a pre-requisite to peer discovery manifesting as technical-mutual aid was the ability of both parties to verify and visualise that backups were being updated regularly and were available across the network, relieving the technical burden and related anxieties for non-technical groups. The usefulness of peer discovery via the CoBox network is therefore dependent on solving these accessibility issues. Trust and collaboration between peers using the CoBox network essentially depends upon the ability to establish the 'health' of the data that is being backed up.

# Key take-aways
* Participating organisations tend to find other likeminded organisations through in-person gatherings. 
* Participating people and organisations are highly networked, but often do not have the time and capacity to meet and share resources.
* Trust between groups / organisations often happens along lines of trust established by a cluster of people. This is often two people, one from each organisation, who trust each other relative to baseline trust between organisations. Based on this trust there is transitive trust that builds between the others in the organisations. 
* The relationships between the trusted nodes is critical until there is a shared history built over time. 
* Trust is used:
    * to clarify miscommunications, misunderstandings and mistakes
    * to build shared language and act as translators between two domains (shared language shorthands, metaphors used, etc.)
* Trust between organisations is often based on likemindedness in terms of values, principles and culture, as well as meeting and interacting in-person.
* Where this is not the case, trust in other organisations and technologies is based on reliability of interaction and availability of assistance. 
* Urgent practical need can override the need for trust and reliability, going with the first best option.
* Trust, in terms of capacity to run infrastructure, is often not only an issue *between* organisations but also *internally*, where one person tends to know the ins and outs of the tech.
* Trust within organisations around new services and technologies is often steered by a champion / advocate.
* A single persistent, dedicated person can shift organisational culture around the adoption of a new technology.
* However, this is often a massive undertaking requiring a lot of energy and advocacy.
    * It is often work which is considered a distraction from core mission 
    * Shifting this culture is very difficult if there is an "old way" of doing things (e.g. moving internal DMS to Signal from Whatsapp)
    * Shifting this culture is easiest when the tool/service being adopted is solving a pain point not addressed by the "old way" (e.g. Jitsi allows video meetings with no account needed and no 40 min time cap and also not needing of software to be downloaded).
* Accessibility, transparency, reliability and automation are highly valued in organisations operating at or beyond capacity. Anything that feels like extra work is not going to fly. 
* There are almost no existing patterns where organisations use services or tech that they have found through a registry or directory (with a few exceptions).
    * The language of registry / directory made people think of the high street pre-internet and old telephone directories. Older participants noted that directories often felt like a stab in the dark because one could not see the reputation of a place. Instead, recommendations played a very important role and people would follow the grapevine for word of mouth good experiences.

# Workshop outline and methodology

The 'peer discovery' needfinding workshops investigated the context, understandings and need for 'peer discovery' as part of the CoBox cooperative cloud.

CoBox operates from a perspective of 'trustfulness'. The intention is to support existing networks of trust, extending this into technical trust and the ability for organisations to mutually back-up one another's data. It is therefore based on one of the major underlying assumptions of many peer-to-peer ecosystems, namely that people are able, find it useful and empowering, to do things on a mutual basis. However, in the technical realm, this is not always the case. For many of the people we spoke with in earlier research, ‘peer-to-peer’ and ‘Open Source’ brought connotations of accessible only to the most technically literate, often lacking the provision of technical support and requiring substantial involvement and extra capacity that most small organisations simply do not have. Accessibility and ease of use were emphasised in the workshops. 

CoBox is a peer-to-peer backup system built on Hypercore. One of the problems the CoBox team wanted to address was how organisations might find each other as 'nodes' or 'partners' to back-up each other's data. The 'peer discovery' needfinding workshops therefore focused on how organisations tend to find each other, what is important to them when when choosing which organisations they collaborate with, how they come to trust one another, as well as the mechanisms in place for assuring and enabling ongoing trust in the infrastructures they use. The workshops also tested the vocabulary that might be used for and in peer-discovery to assess the associations evoked.

**The workshops were guided by these broad questions:**  

* How do organisations tend to find other organisations and begin to collaborate?
* To what extent do peers need to find one-another via the CoBox system?
* What would be meaningful or helpful to know about one another, before entering into a mutual back-up agreement?
* What helps organisations to trust each other?
* What helps organisations to trust technical infrastructures?
      
**The main questions we asked of the participating organisations were:**  

* What would you need to feel willing and capable of backing up another organisation's material? (Technical, informational, social, legal)
* How would you like to be discoverable to other organisations? What kind of information would you be happy to share that would be useful to make a match? 
* Would you prefer to give that information to an organisation making the match (CoBox) or a decentralised network (automated registry)? 
* Would you trust your backups with another organisation? 
* What would make you feel confident about organisations hosting your data?
* How would you go about contacting other groups to back up your data?
* What would you need to know about these organisations?

As is common methodology in User Centered Design, the approach to answering our 'Broad Questions' was to ask wider adjacent questions in an attempt to inform the queries which the project has more specifically. So as not to predetermine / constrict participants' imaginations of what would constitute valuable contributions we decided not to use computer design mockups. Put another way, experience has taught us that when you sit people down at a computer with a UI people often indicate that the options feel fewer and that things are 'more set in stone'.

Instead, the workshops began by asking participants to create their own 'personas', including their roles in their respective organisations and their relationship to technology. 

**Personas**  

Participants were asked to create a persona, aggregating an anonymised model of themselves as a general representation of their organisation or collective:

* Internet native Gen Z; Graphic Designer; works both independently and within a few organisations both as a founder and a contractor.
* Activist proficient with general computer usage and use of some secure tools. Conversely organising over Facebook.
* Radio producer at a community broadcast station that is 40+ years old. Has an intersection with an activist group also present. Overlapping groups.
* Academic. Works both in teams and solo on projects. Constrained by systems which are pre-determined by institutions.
* Artist involved in visual and written arts. Computers used for coordination and general usage. Used to working solo with occasional collaborations. All works are very important.
* Activist founder of a physical and digital archive. Holding lots of physical material, being preserved and digitised in a collective project. Organises the IT – website and hosting. Some ability to design (built website on Joomla) and customise via HTML/PHP. 
* “20th century leftover”. Plays games. Good with Microsoft office, the rest is bad. Social media confuses me. Likes paper. 
* Sleepy activist, if I could be an animal I'd be an owl or eagle. Accidentally looking after the names and contact information of a large part of the local left history and scene, which feels like a scary burden.
* Jack of all trades master of none. Does a lot of tech stuff but without training, not so scared, willing to have a go even if it doesn't work out. Enjoys the amateur-ish approach to technology, other people can do it as well! More interested in digital archive distribution rather than preservation. Reluctantly doing a PhD

**Terminology**  

One workshop made use of a selection of materials conducive to opening the design and imagination space with more of an arts and crafts vibe. Another workshop began by exploring reactions to different words and terminology that we use in CoBox and in discussing 'peer discovery'. This method was intended to bring out associations and experiences relating to these words and phrases. Some examples of these terms:

**Peer**: “A tech term that indicates that something is safer than it would otherwise be – it's like German grammar – someone will explain it to me and then I forget it soon after” ... “I know it's coming from p2p but I'm not necessarily relating it to technology – I would keep the original meaning that someone is your friend.”

**Agreement**: “There are ways of reaching agreement, actually negotiating, or else these types of agreements you see online where you're not aware or involved in the procedure but you have to agree if you want to move on – it is a bit uncomfortable.”
 
**Seed**: “What I'm not doing enough of on those torrent sites I mentioned before.” ... “The initial meaning of the word, is this potential of expansion and of growing, a very good application for these networks. It has philosophical connotations, but it's also a technical thing.” ... “The material that we preserve for the future, what we can learn from the past.”
  
   
The workshops also used roleplaying in order to explore how an organisation might establish contact with another organisation, how trust was established and agreements were entered into and what would happen if and when things went wrong, data was lost or backups were offline. 

We then went through a step-by-step process of how the organisations would go about contacting each other and deciding to back-up for one another, and what they would do should things break down. 

During the workshop development and planning, we felt it was important to leave questions as open as possible so that we could test our assumptions without influencing participants into a set of ideas which had already been decided by our team.

To achieve this, the workshops followed a pattern of moving from 'where participants already are', gathering general experiences, and then refining the focus based on previous answers. Lastly an exit interview was given so that we could gather some final pieces of information and data which we felt related to the domain of P2P and data back up.

Of the participants, about half had attended previous CoBox needfinding workshops, which contributed to our earlier reports [Engineering Trust](https://cobox.cloud/web/cobox-engineering-trust-workshop.pdf) and [CoBox in Context: An overview of data management concerns in a group of co-operative organisations](https://cobox.cloud/web/cobox-in-context-needfinding-report.pdf). The other half were people new to CoBox but from organisations collaborating with the other participants. The workshops were therefore used to explore actual willingness and processes around how these organisations might enter into an agreement to offer and provide technical mutual aid, using CoBox. 

# Familiarity of registry interaction patterns

‘Peer discovery’ relates to other familiar interaction patterns: old phone directories, high streets, as well as, for example, Signal app's pattern of stating who else in one’s address book is also using Signal. 

The older and more established organisations were more likely to use directories. In one workshop, this was a 40+ year old community radio station. The organisation outsource their IT support and infrastructure support for networking and tech as they don't have the expertise internally. They do have tech expertise when it comes to the radio equipment itself, primarily because this is less readily outsourceable.

Of the less established organisations (short-term contracts or more ephemeral collectives) and individual personas (activist, academic, graphic designer) none currently used a registry/directory in their normal operations. In some cases the idea of needing one put them off. If using one, they would seek a recommendation as to the trustworthiness and value of the directory itself from a trusted person or from within a trusted group through which they could get a recommendation, for example from within a special interest / aligned identity facebook group. Google search was referred to as a "kind-of-directory" by the artist. During the production of unique art pieces, they would use it to find materials and resources which they would not normally use (e.g tool libraries, one-off mechanical jobs).

Furthermore, no participants in one workshop could think of examples where a new service was adopted that required discovery via a registry or directory. The participant connected to the radio station suggested that the answer from the station manager may well be different. Likewise the academic mentioned that if you were to speak to someone from within procurement or operations, then the answer may be different as they did not themselves know how "putting things out to tender" worked. They were pretty sure that directories were not used, but rather listings would be made and then organisations would compete and bid to become a vendor.

# Finding collaborating organisations

In one workshop, participants were paired and asked to simulate being in an organisation together. They were asked how they would think about what information they would be willing to offer a party seeking services from their group. They were allowed to draw on the types of people they had in their actual respective organisations, for example the artist and the graphic designer between them had 5 artists, 3 graphic designers and a programmer. They were also asked what information they would request from other groups when deciding if they would engage with and trust them for mutual data-backup or to use data-backup as a service.

**Aggregated list of what people would offer:**  

* company name and business number
* contact email and phone
* skills in house

**Aggregated list of what people would ask:**  

* how long have you been in existence
* how many members / employees do you have
* how much will this cost

Workshop participants and the organisations they came from were highly driven by values and principles, and preferred working with people that shared these. The word ‘likeminded’ however, did not capture this spirit for one set of workshop participants, who felt like they collaborated with many groups and organisations who were in many ways not likeminded (for example different factions of the Left). Nevertheless, there was a sense of a baseline of shared principles that would entail some level of recognition and trust. This was understood as ‘transitive’ trust, where an individual in one organisation knows and trusts another person in a different organisation. This extended to transitive trust by a physical place or context, for example a presence at specific types of gatherings and conferences. Often there were also basic practical reasons that required organisations to collaborate, to do with capacity, distance etc, and word of mouth was a final common way that organisations would find each other.

Overall, there was a strong preference for initially meeting in-person preceding a collaboration, and in-person gatherings seemed to be a common way for organisations and people to know of each other. Two of the participating organisations were independent archives, and so the idea of having material ‘entrusted’ to them was familiar and there was a sense of responsibility around this. 

The willingness to form agreements with organisations depended on shared values and technological capacity, but also - notably - on approaches to copyright. There were worries about take-down notices or other potential issues, as well as emphasis on the need to be able to collaborate with groups that were less encumbered by copyright law. Some participants also expressed insecurity about what should be in an agreement, and would prefer good guidance on what to consider before agreeing on mutual back up arrangements. 

# Trusting tech

There was a distinction made between companies providing tech services, vs services such as data backups provided on a mutual-aid basis. One participant explained the former with an analogy to loo roll: you don’t need to trust it, it just does what it needs to do. This was somewhat associated with paying for a service: “that's just a relationship within capitalism, I give you money and you provide service”. Reliability of service and available, in-person tech support were highly valued for technology and infrastructure. 

Accessibility and ease of use was highly valued. One participant, an independent a digital archive, stated that they liked their server hosting service because it allowed them to roll back to any backup point desired, providing some control over the backend. It “feels like it's been well-designed and made accessible for people like us to use it”. In contrast “‘Open-source stuff that I encounter tends to require a lot more work from me to interact with it, which is hard on my time.” There was a sense of tiredness and operating with limited resources and lack of capacity. 

Capacity in terms of storage and servers is prohibitively expensive for smaller archives. Experiences with server cooperatives in the past were not great. “DIY server projects are good in principle but when they disappear its a real headache”. Having to update things regularly would be a blocker for adoption. 

In terms of technology infrastructure and services, while accessibility, reliability and tech support were the most important aspects, shared principles were nevertheless highly valued. For some of the self-described ‘less technical’ people, there was a sense that one should not trust the tech, but that the situation was a little hopeless: “security is good but it only raises the threshold, if someone wants to hack me they will hack me anyway”. A different participant valued the more self-consciously ‘amateur’ approach of messing around with the tech and knowing the tech providers personally. 

# What would you need to feel confident

It became clear that one of the most important things that would make organisations feel they could trust the infrastructure, themselves and their mutual backup relationships in the CoBox network would be to know the specific state of their back-ups. One participant noted “I would need to know that it was not the only backup – I wouldn't necessarily need to know who was hosting the other backup”. And “isn't the whole point of this that it is distributed so that there is more than one copy - otherwise what is the point of doing it in the first place – it should be automated”.

It was noted that with RAID drives you have more than two backups and that it would be nice to know that there were 3-4 different groups at least backing up one's stuff. The easy roll back to recent back-ups that some virtual servers provide was also highlighted. 

Another noted that it would be great to have permissions. This would allow for the eventuality that if an organisation were to close down or disband, custodianship of specific types of content or records could be handed over to peers, while organisationally sensitive documents might be kept encrypted or entirely deleted.  

# Tech support and capacity

The graphic designer had experience working in a small organisation which has physical printers (Risographs). When there was something wrong with the printers they would Google search to try and figure out the problem using message boards. When they could not figure it out themselves they would contact one of the few technicians still in operation to come out and teach them how to fix this problem next time. The technician would charge for the call out fee, but was motivated and enthused to teach maintenance. They also sold the raw materials used in printing such as the inks, so there was the additional aspect of building towards a more long term relationship.

Each participant knew someone in their collective who was the most experimental when it comes to trying new tools and services. In 50% of the groups we spoke to, a new tool or service was adopted based on a recommendation from such a person. Often there is also one person in the organisation who is technical, and the others not. And many times they underestimate what feels too technical for others. “Several of us were not comfortable using etherpad, but this only became evident after a while.” 

All of the groups had experiences of attempts at getting a new tool or service adopted without success or with the transition stalling through the process at some point and reverting back to the old way of doing things. 

Tech support is ideally also personal, being able to talk things through with people, this needs to be reliable. Personal connections on the level of tech assistance are important. One participant noted that they wouldn't have learned what they have without those relationships. 

What was highlighted as most important was the ability to know when something needed doing or attending to, and what to do or who to contact to fix the problem. With some tools it is obvious when something goes wrong, but with backups, participants communicated a need for easy access to information about the state of their documents or data, and clear indication of when there is  problem. Without this, the state of uncertainty causes a reluctance to trust the software or the mutual aid relationship being used. This applied both to trusting a 'partner' group to backup one's own data, as well as for taking responsibility for hosting backups for another group. In both cases, easy access to the state of the backup was a key factor preceding willingness to engage in a relationship providing digital mutual aid.

# Appendix 1: Additional notes on current back up systems used

Two of the organisations who participated in the co-design workshops shared the following information about the back-up systems they currently use:

**Organisation 1**  

* Database in Access, piped to update on SQL server
* Maintain digi library
* Backups largely maintained by one person
* Hosted by commercial host
* TSO host, bought by Paragon, smallish UK provider

**Organisation 2**  

* use platform called 'leftovers'
* 3 different servers, 2 remote 1 local
* submedia run peertube instance
* have couple of sensitive dbs saved on computer
* would be good to be secure, not on a google document

# Appendix 2: Support

The Peer Discovery workshops and report, as well as implementation of the outcomes, were supported by the Bundesministerium für Bildung und Forschung and the Prototype Fund.
  
![Bundesministerium für Bildung und Forschung - BMBF](./assets/BMBF-logo.png) ![Prototype Fund](./assets/PrototypeFund-logo.png)
